import React from 'react'
import { Card, Radio, Range } from "~elements"

const StartPage: React.FC = () => {
  return (
    <div className={'flex flex-row pt-75 pb-90 px-50'}>
      <Card className={'w-50% mr-33'}>
        <label className={'flex flex-row items-center'}>
          <div>test label</div>
          <Radio name={'1'} onChange={e => console.log('check')} className={'mr-24'} />
        </label>
        <label className={'flex flex-row items-center'}>
          <Radio name={'1'} onChange={e => console.log('check')} />
          <div>another test label</div>
        </label>
        <label className={'flex flex-row items-center'}>
          <Range />
        </label>
      </Card>
      <Card className={'w-50% bg-blue-gradient text-white'}>
        <h1>Леха пидор =)) ХД</h1>
      </Card>
    </div>
  );
};

export default StartPage;