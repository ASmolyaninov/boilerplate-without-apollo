import React from 'react'

interface MainLayoutProps {

}

const MainLayout: React.FC<MainLayoutProps> = props => {
  return (
    <div className={'mx-auto max-w-1144 w-100% h-screen'}>
      {props.children}
    </div>
  )
}

export default MainLayout