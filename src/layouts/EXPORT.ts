import loadable from "@loadable/component"

export const MainLayout = loadable(() => import('./MainLayout'))