import React from 'react'

interface CardProps {
  children: JSX.Element | JSX.Element[] | string
  className?: string
}

const Card: React.FC<CardProps> = props => {
  const { children, className } = props

  return (
    <div className={'rounded-10 bg-white shadow p-50 ' + className}>
      {children}
    </div>
  )
}

export default Card