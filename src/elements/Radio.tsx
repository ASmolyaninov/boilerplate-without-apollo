import React, { ChangeEventHandler } from 'react'

interface RadioProps {
  className?: string
  checked?: boolean
  name?: string
  disabled?: boolean
  onChange?: ChangeEventHandler<HTMLInputElement>
}

const Radio: React.FC<RadioProps> = props => {
  const { className, ...inputProps } = props

  return (
    <label className={"radio-wrapper radio " + className}>
      <input type="radio" {...inputProps} />
      <div className="radio-indicator" />
    </label>
  )
}

export default Radio