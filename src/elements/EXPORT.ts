import loadable from '@loadable/component'

export const Card = loadable(() => import('./Card'))
export const Radio = loadable(() => import('./Radio'))
export const Range = loadable(() => import('./Range'))
