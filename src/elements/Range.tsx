import React, { useMemo, useRef, useState } from 'react'
import { useLongPress } from '~hooks'
import { Minus, Plus } from '~icons'

interface RangeProps {
  onChange?: (value: string) => void
  min?: number
  max?: number
  defaultValue?: number
}

const getRangeProgressPercent = (value: number, min?: number, max?: number): number => {
  let progress = value
  if (typeof max === 'number' && typeof min === 'number') {
    progress = (value - min) / (max - min) * 100
  }

  return progress
}

const Range: React.FC<RangeProps> = props => {
  const { onChange, ...inputProps } = props
  const [progressPercent, setProgressPercent] = useState<number>(50)
  const rangeRef = useRef<HTMLInputElement>(null)
  const intervalRef = useRef<ReturnType<typeof setInterval>>(null)
  const onePercentOfRange = useMemo(() => (inputProps.min && inputProps.max) ? (inputProps.max - inputProps.min)/100 : 1, [inputProps.min, inputProps.max])

  const increaseValue = () => {
    if (rangeRef.current) {
      const value = rangeRef.current.value
      let newValue = +value + onePercentOfRange
      const isValidRangeBorders = typeof inputProps.min === 'number' && typeof  inputProps.max === 'number'
      const maxValue = isValidRangeBorders ? inputProps.max! - inputProps.min! : 100
      if (newValue > maxValue) {
        newValue = maxValue
      }

      handleChange(newValue.toString())
      rangeRef.current.value = newValue.toString()
    }
  }

  const decreaseValue = () => {
    if (rangeRef.current) {
      const value = rangeRef.current.value
      let newValue = +value - onePercentOfRange
      const isValidRangeBorders = typeof inputProps.min === 'number' && typeof  inputProps.max === 'number'
      const minValue = isValidRangeBorders ? inputProps.min! : 100
      if (newValue > minValue) {
        newValue = minValue
      }

      handleChange(newValue.toString())
      rangeRef.current.value = newValue.toString()
    }
  }

  const onLongPress = (type: 'increase' | 'decrease') => () => {
    let interval = 300
    const exponentialTimerFunction = function(){
      if (interval > 20) {
        interval *= 0.5
      }

      if (type === 'increase') increaseValue()
      if (type === 'decrease') decreaseValue()
      intervalRef.current = setTimeout(exponentialTimerFunction, interval)
    }
    intervalRef.current = setTimeout(exponentialTimerFunction, interval)
  }

  const onLongPressEnds = () => {
    if (intervalRef.current) {
      clearTimeout(intervalRef.current)
    }
  }

  const handleClick = (type: 'increase' | 'decrease') => () => {
    if (type === 'increase') increaseValue()
    if (type === 'decrease') decreaseValue()
  }

  const defaultOptions = {
    shouldPreventDefault: true,
    delay: 250,
  };
  const minusLongPressEvent = useLongPress(onLongPress('decrease'), onLongPressEnds, handleClick('decrease'), defaultOptions)
  const plusLongPressEvent = useLongPress(onLongPress('increase'), onLongPressEnds, handleClick('increase'), defaultOptions)

  const handleChange = (value: string) => {
    const progress = getRangeProgressPercent(+value, inputProps.min, inputProps.max)
    rangeRef.current!.style.background = 'linear-gradient(to right, transparent 0%, transparent ' + progress + '%, #C4C4C4 ' + progress + '%, #C4C4C4 100%), linear-gradient(321.96deg, #14539B 10.47%, #115BA3 23.82%, #0B74BA 45.49%, #009BDF 72.83%, #009FE3 74.9%)'
    setProgressPercent(progress)
    onChange && onChange(value)
  }

  const thumbTagLeftPosition = `calc(${progressPercent}% - ${32*(progressPercent/100) - 32}px)`
  const thumbTagTransform = `translateX(calc(${-50}% - ${32*(progressPercent/100)}px))`
  return (
    <div className={'flex flex-row'}>
      <div
        className={'flex items-center justify-center flex-grow rounded-50% bg-purple-gradient min-w-50 min-h-50 cursor-pointer'}
        {...minusLongPressEvent}
      >
        <Minus className={'text-white'} />
      </div>
      <div className={'relative flex items-center mx-25'}>
        <div className={'absolute px-28 py-8 bg-purple-gradient bottom-60 rounded-30 text-white'} style={{ left: thumbTagLeftPosition, transform: thumbTagTransform }}>123</div>
        <input ref={rangeRef} className={'range'} type="range" onChange={e => handleChange(e.currentTarget.value)} {...inputProps} />
      </div>
      <div
        className={'flex items-center justify-center flex-grow rounded-50% bg-purple-gradient min-w-50 min-h-50 cursor-pointer'}
        {...plusLongPressEvent}
      >
        <Plus className={'text-white'} />
      </div>
    </div>
  )
}

export default Range