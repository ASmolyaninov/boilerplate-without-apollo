import React, { useCallback, useRef, useState } from "react"

const useLongPress = <T = React.MouseEvent>(
  onLongPress: (e: T) => void,
  onLongPressEnds: () => void,
  onClick?: () => void,
  { shouldPreventDefault = true, delay = 300 } = {}
) => {
  const [longPressTriggered, setLongPressTriggered] = useState(false)
  const timeout = useRef<ReturnType<typeof setTimeout>>()
  const target = useRef<HTMLElement>()

  const start = useCallback(
    event => {
      if (shouldPreventDefault && event.target) {
        event.target.addEventListener("touchend", preventDefault, {
          passive: false
        })
        target.current = event.target
      }
      onClick && onClick()
      timeout.current = setTimeout(() => {
        onLongPress(event)
        setLongPressTriggered(true)
      }, delay)
    },
    [onLongPress, delay, shouldPreventDefault]
  )

  const clear = useCallback(
    (event, shouldTriggerClick = true) => {
      timeout.current && clearTimeout(timeout.current)
      onLongPressEnds()
      setLongPressTriggered(false)
      if (shouldPreventDefault && target.current) {
        target.current.removeEventListener("touchend", preventDefault)
      }
    },
    [shouldPreventDefault, longPressTriggered]
  )

  return {
    onMouseDown: (e: T) => start(e),
    onTouchStart: (e: React.TouchEvent) => start(e),
    onMouseUp: (e: T) => clear(e),
    onMouseLeave: (e: T) => clear(e, false),
    onTouchEnd: (e: React.TouchEvent) => clear(e)
  }
}

const isTouchEvent = (event: React.TouchEvent) => {
  return "touches" in event
}

const preventDefault = (event: TouchEvent) => {
  if (!isTouchEvent(event as unknown as React.TouchEvent)) return

  if (event.touches.length < 2 && event.preventDefault) {
    event.preventDefault()
  }
}

export default useLongPress