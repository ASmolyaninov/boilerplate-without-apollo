/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.com/docs/node-apis/
 */

const path = require('path')

exports.createPages = async ({ page, actions }) => {
  const { createPage } = actions
  // const page = path.resolve(`src/pages/page`)
  //
  // createPage({
  //   path: '/page',
  //   component: Page
  // })
}

exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions

  // page.matchPath is a special key that's used for matching pages
  // only on the client.
  if (page.path.match(/^\/admin/)) {
    page.matchPath = '/admin/*'

    // Update the page.
    createPage(page)
  }
}

exports.onCreateWebpackConfig = function ({ stage, loaders, actions, plugins }) {
  const config = {
    module: {
      rules: []
    },
    resolve: {
      alias: {
        '~layouts': path.resolve(__dirname, 'src/layouts/EXPORT.ts'),
        '~elements': path.resolve(__dirname, 'src/elements/EXPORT.ts'),
        '~hooks': path.resolve(__dirname, 'src/hooks/EXPORT.ts'),
        '~icons': path.resolve(__dirname, 'src/elements/icons/EXPORT.tsx'),
        '~styles': path.resolve(__dirname, 'static/assets/styles'),
      }
    }
  }

  if (stage === 'build-html') {
    /* For libs that should be ensured that window and document exists */
    // config.module.rules.push({
    //   test: /bad-module/ /* Define your lib */,
    //   use: loaders.null()
    // })
  }

  actions.setWebpackConfig(config)
}
