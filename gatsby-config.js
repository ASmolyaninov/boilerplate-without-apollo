module.exports = {
  siteMetadata: {
    siteUrl: "https://www.yourdomain.tld",
    title: "Fluent Mortgage Repayment Tool",
  },
  plugins: [
    'gatsby-plugin-typescript',
    'gatsby-plugin-postcss'
  ],
};