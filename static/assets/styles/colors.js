module.exports = {
  current: 'currentColor',
  transparent: 'transparent',
  white: '#FFFFFF',
  black: '#000000'
}
